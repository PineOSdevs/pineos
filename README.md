# auroraOS
An operating system that is based on arch. It has a very nice installer.
It was wrote using whiptail and it pre-configures alot of the OS.


## Getting started

Firstly, you need to download the Arch ISO from the website : https://archlinux.org/download/

Next I recommend that you use ethernet but wifi is also a good choice. 

Finally, boot up the iso and type this :

 ```
 pacman -Syy git && git clone https://gitlab.com/auroraOSdevs/auroraOS.git && cd auroraOS
 ```
This command downloads and installs git and PineOS Installer


Next run this one and that runs the installer:
```
chmod +x aos-p1.sh && ./aos-p1.sh
```
Now you just run through to install and your done!


## Screenshots
![Screenshot_2021-10-31_12-15-22](https://user-images.githubusercontent.com/76793908/139582798-9128ea6a-8227-478a-bf0a-250f01078364.png)
![Screenshot from 2021-10-21 19-54-57](https://user-images.githubusercontent.com/76793908/139582854-92ae1ddf-9a0c-4a34-92e3-99af169c3604.png)
![Screenshot_2021-10-31_09-36-35](https://user-images.githubusercontent.com/76793908/139582863-fb851297-b35b-4cb4-b7af-87f690804d53.png)
