#!/bin/sh
# PineOS Developers
# 2021-2022 yoda

clear
xfconf-query -c xsettings -p /Net/ThemeName -s "Arc-Dark"
xfconf-query -c xfwm4 -p /general/theme -s "Arc-Dark"
xfconf-query -c xsettings -p /Net/IconThemeName -s "Papirus-Dark"
feh --bg-scale /usr/share/backgrounds/xfce/PineOS-Sierra.png
clear
echo -e "\e[1;4m Welcome to PineOS 0.0.8\e[1;0m" 
echo -e "\e[1;33m      *"
echo -e "\e[1;32m     ###"
echo -e "\e[1;32m    #####"
echo -e "\e[1;32m   #######      Enjoy PineOS and join our discord"
echo -e "\e[1;32m  #########"
echo -e "\e[1;32m ###########\e[1;0m"
echo -e "     !!!"
echo -e "     !!!"
echo " "
echo " Discord : https://discord.gg/xdBJQV52"
read -p " [PRESS ANY KEY TO EXIT]" quit
exit
